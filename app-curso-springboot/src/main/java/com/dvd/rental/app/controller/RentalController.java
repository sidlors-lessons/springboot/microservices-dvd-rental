package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Rental;
import com.dvd.rental.app.service.RentalService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rentals")
@Tag(name = "Operational", description = "Rental CRUD endpoints")
public class RentalController {
    
    private final RentalService rentalService;
    
    public RentalController(RentalService rentalService) {
        this.rentalService = rentalService;
    }
    
    /*@GetMapping
    @Operation(summary = "Get a list of all rentals")
    public ResponseEntity<List<Rental>> getAllRentals() {
        List<Rental> rentals = rentalService.getAllRentals();
        return ResponseEntity.ok(rentals);
    }*/



    @GetMapping
    @Operation(summary = "Get a paginated list of Rental")
    public ResponseEntity<Page<Rental>> getPaginatedRental(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size
    ) {
        Page<Rental> rentals = rentalService.getPaginatedPayments(page, size);
        return ResponseEntity.ok(rentals);
    }
    
    @GetMapping("/{id}")
    @Operation(summary = "Get a rental by ID")
    public ResponseEntity<Rental> getRentalById(@PathVariable @Parameter(description = "Rental ID") Integer id) {
        Optional<Rental> rental = rentalService.getRentalById(id);
        return rental.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
    
    @PostMapping
    @Operation(summary = "Create a new rental")
    public ResponseEntity<Rental> createRental(@Valid @RequestBody Rental rental) {
        Rental createdRental = rentalService.createRental(rental);
        return ResponseEntity.ok(createdRental);
    }
    
    @PutMapping("/{id}")
    @Operation(summary = "Update a rental by ID")
    public ResponseEntity<Rental> updateRental(
            @PathVariable @Parameter(description = "Rental ID") Integer id,
            @Valid @RequestBody Rental rental
    ) {
        Rental updatedRental = rentalService.updateRental(id, rental);
        return ResponseEntity.ok(updatedRental);
    }
    
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a rental by ID")
    public ResponseEntity<Void> deleteRental(@PathVariable @Parameter(description = "Rental ID") Integer id) {
        rentalService.deleteRental(id);
        return ResponseEntity.noContent().build();
    }
}
