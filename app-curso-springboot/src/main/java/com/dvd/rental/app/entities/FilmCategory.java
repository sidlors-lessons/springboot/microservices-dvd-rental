package com.dvd.rental.app.entities;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "film_category")
public class FilmCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private FilmCategoryPK id;

    @Column(name = "last_update")
    private Timestamp lastUpdate;

    public FilmCategory() {
    }

    public FilmCategoryPK getId() {
        return id;
    }

    public void setId(FilmCategoryPK id) {
        this.id = id;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilmCategory that = (FilmCategory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(lastUpdate, that.lastUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lastUpdate);
    }
}
