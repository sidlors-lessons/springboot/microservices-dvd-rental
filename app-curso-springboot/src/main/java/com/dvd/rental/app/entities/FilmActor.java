package com.dvd.rental.app.entities;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "film_actor")
@Getter
@Setter
public class FilmActor implements Serializable{

	@EmbeddedId
	private FilmActorPK id;

	@ManyToOne
	@MapsId("actorId")
	@JoinColumn(name = "actor_id", referencedColumnName = "actor_id")
	private Actor actor;

	@ManyToOne
	@MapsId("filmId")
	@JoinColumn(name = "film_id", referencedColumnName = "film_id")
	private Film film;
	
	@Column(name = "last_update", nullable = false)
	private Timestamp lastUpdate;

	/*public FilmActor() {
		this.lastUpdate = new Timestamp(System.currentTimeMillis());
	}

	public FilmActorPK getId() {
		return id;
	}

	public void setId(FilmActorPK id) {
		this.id = id;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}*/
}