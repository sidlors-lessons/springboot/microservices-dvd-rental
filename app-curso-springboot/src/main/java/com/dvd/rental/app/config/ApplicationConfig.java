package com.dvd.rental.app.config;

import jakarta.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.client.RestTemplate;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;


@Configuration
@SecurityScheme( 
    name = "bearerAuth", 
    type = SecuritySchemeType.HTTP, 
    bearerFormat = "JWT", 
    scheme = "bearer" 
) 
public class ApplicationConfig {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    public OpenAPI customOpenAPI ( @Value ( "${application-description}" ) String appDesciption , 
                                    @Value ( "${application-version}" ) String appVersion ) {
        return new OpenAPI ( )
                .info ( new Info ( )
                        .title ( "Dvd Rental Online application API" )
                        .version ( appVersion )
                        .description ( appDesciption )
                        .termsOfService ( "http://swagger.io/terms/" )
                        .license ( new License ( ).name ( "Apache 2.0" ).url ( "http://springdoc.org" ) ) );

    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    

    
}