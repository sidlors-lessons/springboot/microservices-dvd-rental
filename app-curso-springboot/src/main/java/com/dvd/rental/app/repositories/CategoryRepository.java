package com.dvd.rental.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.dvd.rental.app.entities.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

}