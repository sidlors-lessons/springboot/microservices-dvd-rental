package com.dvd.rental.app.controller;

import java.net.URI;
import java.util.List;

import jakarta.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dvd.rental.app.entities.FilmCategory;
import com.dvd.rental.app.service.FilmCategoryService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/film-categories")
@Tag(name = "Catalog", description = "Film Category Catalog CRUD endpoints")
public class FilmCategoryController {

    private final FilmCategoryService filmCategoryService;

    public FilmCategoryController(FilmCategoryService filmCategoryService) {
        this.filmCategoryService = filmCategoryService;
    }

    @GetMapping
    @Operation(summary = "Get a list of all film categories")
    public ResponseEntity<List<FilmCategory>> getAllFilmCategories() {
        List<FilmCategory> filmCategories = filmCategoryService.getAllFilmCategories();
        return ResponseEntity.ok(filmCategories);
    }

    @GetMapping("/{filmId}/{categoryId}")
    @Operation(summary = "Get a film category by film ID and category ID")
    public ResponseEntity<FilmCategory> getFilmCategoryById(
            @PathVariable @Parameter(description = "Film ID") Short filmId,
            @PathVariable @Parameter(description = "Category ID") Short categoryId
    ) {
        FilmCategory filmCategory = filmCategoryService.getFilmCategoryById(filmId, categoryId);
        if (filmCategory != null) {
            return ResponseEntity.ok(filmCategory);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create a new film category")
    public ResponseEntity<FilmCategory> createFilmCategory(@Valid @RequestBody FilmCategory filmCategory) {
        FilmCategory createdFilmCategory = filmCategoryService.createFilmCategory(filmCategory);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{filmId}/{categoryId}")
                .buildAndExpand(createdFilmCategory.getId(), createdFilmCategory.getId())
                .toUri();
        return ResponseEntity.created(location).body(createdFilmCategory);
    }

    @PutMapping("/{filmId}/{categoryId}")
    @Operation(summary = "Update a film category by film ID and category ID")
    public ResponseEntity<FilmCategory> updateFilmCategory(
            @PathVariable @Parameter(description = "Film ID") Short filmId,
            @PathVariable @Parameter(description = "Category ID") Short categoryId,
            @Valid @RequestBody FilmCategory filmCategoryDetails
    ) {
        FilmCategory updatedFilmCategory = filmCategoryService.updateFilmCategory(filmId, categoryId, filmCategoryDetails);
        if (updatedFilmCategory != null) {
            return ResponseEntity.ok(updatedFilmCategory);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{filmId}/{categoryId}")
    @Operation(summary = "Delete a film category by film ID and category ID")
    public ResponseEntity<Void> deleteFilmCategory(
            @PathVariable @Parameter(description = "Film ID") Short filmId,
            @PathVariable @Parameter(description = "Category ID") Short categoryId
    ) {
        boolean deleted = filmCategoryService.deleteFilmCategory(filmId, categoryId);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
