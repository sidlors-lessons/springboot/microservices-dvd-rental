package com.dvd.rental.app.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryVO {

    private Integer categoryId;
    private String name;
    
    
}
