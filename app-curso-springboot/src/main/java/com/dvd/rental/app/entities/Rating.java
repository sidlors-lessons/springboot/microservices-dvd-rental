package com.dvd.rental.app.entities;

import java.io.Serializable;

import jakarta.persistence.Embeddable;


@Embeddable
public class Rating implements Serializable{

    

    private String rating;

    public Rating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating){
        this.rating = rating;
    }




}