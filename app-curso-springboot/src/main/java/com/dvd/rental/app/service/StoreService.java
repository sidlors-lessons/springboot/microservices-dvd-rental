package com.dvd.rental.app.service;


import com.dvd.rental.app.entities.Store;
import com.dvd.rental.app.repositories.StoreRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreService {

    private final StoreRepository storeRepository;

    public StoreService(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    public List<Store> getAllStores() {
        return storeRepository.findAll();
    }

    public Optional<Store> getStoreById(Integer id) {
        return storeRepository.findById(id);
    }

    public Store createStore(Store store) {
        return storeRepository.save(store);
    }

    public Store updateStore(Integer id, Store storeDetails) {
        Store store = getStoreById(id).orElse(null);
        if (store != null) {
            store.setManagerStaffId(storeDetails.getManagerStaffId());
            store.setAddressId(storeDetails.getAddressId());
            store.setLastUpdate(storeDetails.getLastUpdate());
            return storeRepository.save(store);
        }
        return null;
    }

    public void deleteStore(Integer id) {
        storeRepository.deleteById(id);
    }
}
