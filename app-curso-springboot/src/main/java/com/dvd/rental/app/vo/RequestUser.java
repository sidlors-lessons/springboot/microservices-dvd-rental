package com.dvd.rental.app.vo;

import lombok.Data;

@Data
public class RequestUser {

    private String  username;
    private String password;

    
}
