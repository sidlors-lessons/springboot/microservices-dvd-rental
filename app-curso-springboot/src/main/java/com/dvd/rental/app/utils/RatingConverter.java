package com.dvd.rental.app.utils;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import com.dvd.rental.app.entities.Rating;
import lombok.extern.slf4j.Slf4j;

@Converter(autoApply = true)
@Slf4j
public class RatingConverter implements AttributeConverter<Rating, String>{
 
    @Override
    public String convertToDatabaseColumn(Rating rating) {
        if (rating == null) {
            return null;
        }
        return rating.getRating().replace("_", "-");
    }

    @Override
    public Rating convertToEntityAttribute(String rating) {
        log.info("rating: ",rating);
        if (rating == null) {
            
            return null;
        }
        rating = rating.replace("-", "_");
        return new Rating(rating);
    }
}