package com.dvd.rental.app.service;
import java.util.List;

import com.dvd.rental.app.entities.Country;

public interface CountryService {

    List<Country> getAllCountries();

    Country getCountryById(Integer id);

    Country addCountry(Country country);

    Country updateCountry(Integer id, Country country);

    void deleteCountry(Integer id);
}
