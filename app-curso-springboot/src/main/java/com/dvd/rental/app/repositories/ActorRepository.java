package com.dvd.rental.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dvd.rental.app.entities.Actor;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {
}