package com.dvd.rental.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dvd.rental.app.entities.Category;
import com.dvd.rental.app.mapper.CategoryMapper;
import com.dvd.rental.app.repositories.CategoryRepository;
import com.dvd.rental.app.vo.CategoryVO;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CategoryService {


    @Autowired
    private CategoryRepository categoryRepository;

    public List<CategoryVO> getAllCategories() {
        log.info("Get all categories in DB");

        List<CategoryVO> listVO= new ArrayList<CategoryVO>();

        for (Category category : categoryRepository.findAll()) {
            listVO.add(CategoryMapper.INSTANCIA.toVO(category));
        }

        return  listVO;
    }

    public Optional<Category> getCategoryById(Integer categoryId) {
        return categoryRepository.findById(categoryId);
    }

    public Category saveCategory(Category category) {
        return categoryRepository.save(category);
    }

    public void deleteCategoryById(Integer categoryId) {
        categoryRepository.deleteById(categoryId);
    }
}