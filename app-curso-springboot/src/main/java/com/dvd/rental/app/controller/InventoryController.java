package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Inventory;
import com.dvd.rental.app.service.InventoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/inventories")
@Tag(name = "Operational", description = "Inventory Management Endpoints")
public class InventoryController {

    private final InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping
    @Operation(summary = "Get all inventories")
    public ResponseEntity<List<Inventory>> getAllInventories() {
        List<Inventory> inventories = inventoryService.getAllInventories();
        return ResponseEntity.ok(inventories);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get an inventory by ID")
    public ResponseEntity<Inventory> getInventoryById(@PathVariable @Parameter(description = "Inventory ID") Integer id) {
        Optional<Inventory> inventory = inventoryService.getInventoryById(id);
        return inventory.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @Operation(summary = "Create a new inventory")
    public ResponseEntity<Inventory> createInventory(@RequestBody Inventory inventory) {
        Inventory createdInventory = inventoryService.saveInventory(inventory);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdInventory);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update an inventory by ID")
    public ResponseEntity<Inventory> updateInventory(
            @PathVariable @Parameter(description = "Inventory ID") Integer id,
            @RequestBody Inventory inventoryDetails) {
        Optional<Inventory> existingInventory = inventoryService.getInventoryById(id);
        if (existingInventory.isPresent()) {
            Inventory updatedInventory = inventoryService.saveInventory(inventoryDetails);
            return ResponseEntity.ok(updatedInventory);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete an inventory by ID")
    public ResponseEntity<Void> deleteInventory(@PathVariable @Parameter(description = "Inventory ID") Integer id) {
        Optional<Inventory> existingInventory = inventoryService.getInventoryById(id);
        if (existingInventory.isPresent()) {
            inventoryService.deleteInventory(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
