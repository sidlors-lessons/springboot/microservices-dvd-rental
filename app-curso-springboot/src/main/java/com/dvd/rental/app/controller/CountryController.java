package com.dvd.rental.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dvd.rental.app.entities.Country;
import com.dvd.rental.app.service.CountryService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

@RestController
@RequestMapping("/countries")
@Tag(name = "Catalog", description = "Catalogs CRUD endpoints")
public class CountryController {

    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("")
    @Operation(summary = "Get all countries")
    @ApiResponse(responseCode = "200", description = "List of countries",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Country.class)))
    public ResponseEntity<List<Country>> getAllCountries() {
        List<Country> countries = countryService.getAllCountries();
        return ResponseEntity.ok(countries);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a country by ID")
    @ApiResponse(responseCode = "200", description = "Found the country",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Country.class)))
    @ApiResponse(responseCode = "404", description = "Country not found")
    public ResponseEntity<Country> getCountryById(@PathVariable @Parameter(description = "Country ID") Integer id) {
        Country country = countryService.getCountryById(id);
        if (country == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(country);
    }

    @PostMapping("/")
    @Operation(summary = "Create a new country")
    @ApiResponse(responseCode = "201", description = "Country created",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Country.class)))
    public ResponseEntity<Country> createCountry(@RequestBody Country country) {
        Country createdCountry = countryService.addCountry(country);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdCountry);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a country by ID")
    @ApiResponse(responseCode = "200", description = "Country updated",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Country.class)))
    @ApiResponse(responseCode = "404", description = "Country not found")
    public ResponseEntity<Country> updateCountry(@PathVariable("id") @Parameter(description = "Country ID") int id, @RequestBody Country country) {
        Country updatedCountry = countryService.updateCountry(id, country);
        if (updatedCountry != null) {
            return ResponseEntity.ok(updatedCountry);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a country by ID")
    @ApiResponse(responseCode = "204", description = "Country deleted")
    @ApiResponse(responseCode = "404", description = "Country not found")
    public ResponseEntity<Void> deleteCountry(@PathVariable("id") @Parameter(description = "Country ID") int id) {
        countryService.deleteCountry(id);
        return ResponseEntity.noContent().build();
    }
}
