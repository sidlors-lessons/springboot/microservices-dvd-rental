package com.dvd.rental.app.service;

import com.dvd.rental.app.entities.FilmCategory;
import com.dvd.rental.app.entities.FilmCategoryPK;
import com.dvd.rental.app.repositories.FilmCategoryRepository;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FilmCategoryService {

    private final FilmCategoryRepository filmCategoryRepository;

    public FilmCategoryService(FilmCategoryRepository filmCategoryRepository) {
        this.filmCategoryRepository = filmCategoryRepository;
    }

    public List<FilmCategory> getAllFilmCategories() {
        return filmCategoryRepository.findAll();
    }

    public FilmCategory getFilmCategoryById(Short filmId, Short categoryId) {
        Optional<FilmCategory> optionalFilmCategory = filmCategoryRepository.findById(new FilmCategoryPK(filmId, categoryId));
        return optionalFilmCategory.orElse(null);
    }

    public FilmCategory createFilmCategory(FilmCategory filmCategory) {
        return filmCategoryRepository.save(filmCategory);
    }

    public FilmCategory updateFilmCategory(Short filmId, Short categoryId, FilmCategory filmCategoryDetails) {
        Optional<FilmCategory> optionalFilmCategory = filmCategoryRepository.findById(new FilmCategoryPK(filmId, categoryId));
        if (optionalFilmCategory.isPresent()) {
            FilmCategory filmCategory = optionalFilmCategory.get();
            filmCategory.setLastUpdate(filmCategoryDetails.getLastUpdate());
            return filmCategoryRepository.save(filmCategory);
        } else {
            return null;
        }
    }

    public boolean deleteFilmCategory(Short filmId, Short categoryId) {
        Optional<FilmCategory> optionalFilmCategory = filmCategoryRepository.findById(new FilmCategoryPK(filmId, categoryId));
        if (optionalFilmCategory.isPresent()) {
            filmCategoryRepository.delete(optionalFilmCategory.get());
            return true;
        } else {
            return false;
        }
    }
}
