package com.dvd.rental.app.controller;

import java.net.URI;
import java.util.List;

import jakarta.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dvd.rental.app.entities.Address;
import com.dvd.rental.app.service.AddressService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/addresses")
@Tag(name = "Catalog", description = "Address Catalog CRUD endpoints")
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    @Operation(summary = "Get a list of all addresses")
    public ResponseEntity<List<Address>> getAllAddresses() {
        List<Address> addresses = addressService.getAllAddresses();
        return ResponseEntity.ok(addresses);
    }

    @GetMapping("/{addressId}")
    @Operation(summary = "Get an address by ID")
    public ResponseEntity<Address> getAddressById(
            @PathVariable @Parameter(description = "Address ID") Integer addressId
    ) {
        Address address = addressService.getAddressById(addressId);
        if (address != null) {
            return ResponseEntity.ok(address);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create a new address")
    public ResponseEntity<Address> createAddress(@Valid @RequestBody Address address) {
        Address createdAddress = addressService.createAddress(address);
        URI location = URI.create("/addresses/" + createdAddress.getAddressId());
        return ResponseEntity.created(location).body(createdAddress);
    }

    @PutMapping("/{addressId}")
    @Operation(summary = "Update an address by ID")
    public ResponseEntity<Address> updateAddress(
            @PathVariable @Parameter(description = "Address ID") Integer addressId,
            @Valid @RequestBody Address addressDetails
    ) {
        Address updatedAddress = addressService.updateAddress(addressId, addressDetails);
        if (updatedAddress != null) {
            return ResponseEntity.ok(updatedAddress);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{addressId}")
    @Operation(summary = "Delete an address by ID")
    public ResponseEntity<Void> deleteAddress(
            @PathVariable @Parameter(description = "Address ID") Integer addressId
    ) {
        boolean deleted = addressService.deleteAddress(addressId);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}