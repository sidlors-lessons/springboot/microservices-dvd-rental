package com.dvd.rental.app.repositories;


import com.dvd.rental.app.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
    // Additional query methods if needed
}
