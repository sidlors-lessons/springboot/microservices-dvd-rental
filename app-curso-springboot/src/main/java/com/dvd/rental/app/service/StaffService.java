// StaffService.java (Service)
package com.dvd.rental.app.service;

import com.dvd.rental.app.entities.Staff;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.repositories.StaffRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class StaffService {
    
    private final StaffRepository staffRepository;
    
    public StaffService(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }
    
    public List<Staff> getAllStaff() {
        return staffRepository.findAll();
    }
    
    public Staff getStaffById(Integer id) throws DataNotFoundException {
        return staffRepository.findById(id).orElseThrow(() -> new DataNotFoundException( "staffId"));
    }
    
    public Staff createStaff(Staff staff) {
        return staffRepository.save(staff);
    }
    
    public Staff updateStaff(Integer id, Staff staff)throws DataNotFoundException {
        staff.setStaffId(id);
        if (!staffRepository.existsById(id)) {
            throw new DataNotFoundException("id");
        }
        return staffRepository.save(staff);
    }
    
    public void deleteStaff(Integer id)throws DataNotFoundException {

        if (!staffRepository.existsById(id)) {
            throw new DataNotFoundException("id");
        }
        staffRepository.deleteById(id);
        
    }
}
