package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Store;
import com.dvd.rental.app.service.StoreService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/stores")
@Tag(name = "Catalog", description = "Store CRUD endpoints")
public class StoreController {

    private final StoreService storeService;

    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping
    @Operation(summary = "Get a list of all stores")
    public ResponseEntity<List<Store>> getAllStores() {
        List<Store> stores = storeService.getAllStores();
        return ResponseEntity.ok(stores);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a store by ID")
    public ResponseEntity<Store> getStoreById(
            @PathVariable @Parameter(description = "Store ID") Integer id) {
        Optional<Store> store = storeService.getStoreById(id);
        return store.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @Operation(summary = "Create a new store")
    public ResponseEntity<Store> createStore(@RequestBody Store store) {
        Store createdStore = storeService.createStore(store);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdStore.getStoreId())
                .toUri();
        return ResponseEntity.created(location).body(createdStore);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a store by ID")
    public ResponseEntity<Store> updateStore(
            @PathVariable @Parameter(description = "Store ID") Integer id,
            @RequestBody Store storeDetails) {
        Store updatedStore = storeService.updateStore(id, storeDetails);
        if (updatedStore != null) {
            return ResponseEntity.ok(updatedStore);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a store by ID")
    public ResponseEntity<Void> deleteStore(
            @PathVariable @Parameter(description = "Store ID") Integer id) {
        storeService.deleteStore(id);
        return ResponseEntity.noContent().build();
    }
}
