package com.dvd.rental.app.service;

import java.util.List;

import com.dvd.rental.app.entities.LdapUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ldap.query.LdapQueryBuilder;


@Service
@Slf4j
public class LdapUserServiceImpl {

	@Autowired
	private LdapTemplate ldapTemplate;

	public List<LdapUser> getAllUsers() {
		return ldapTemplate.findAll(LdapUser.class);
	}

	public LdapUser getUserByUid(String uid) {
		return ldapTemplate.findOne(
				LdapQueryBuilder.query().where("uid").is(uid),
				LdapUser.class
		);
	}

	public boolean validateUserAndPass(String username, String password) {
		try {
			// Filtrar por nombre de usuario y password
			String filter = "(&(objectClass=inetOrgPerson)(uid=" + username + ")(userPassword=" + password + "))";

			// Consulta LDAP
			boolean userValid = ldapTemplate.authenticate("", filter, password);

			return userValid;
		} catch (Exception e) {
			e.printStackTrace(); // Manejo de errores según tus necesidades
			return false;
		}
	}

	public LdapUser createUser(LdapUser user) {
		ldapTemplate.create(user);
		return user;
	}

	public LdapUser updateUser(LdapUser user) {
		ldapTemplate.update(user);
		return user;
	}

	public boolean deleteUser(String uid) {
		// Realizar la búsqueda del usuario en OpenLDAP
		LdapUser ldapUser = getUserByUid(uid);

		if (ldapUser != null) {
			// El usuario existe, proceder con la eliminación
			try {
				ldapTemplate.unbind(buildDn(uid));
				return true;  // Eliminación exitosa
			} catch (Exception e) {
				// Manejar la excepción, por ejemplo, loguearla
				e.printStackTrace();
				return false; // Error al eliminar el usuario
			}
		} else {
			// El usuario no existe
			return false;
		}
	}

	private String buildDn(String uid) {
		// Implementa la lógica para construir el Distinguished Name (DN) aquí
		// Por ejemplo:
		return "uid=" + uid + ",ou=users,dc=mycompany,dc=com";
	}
}
