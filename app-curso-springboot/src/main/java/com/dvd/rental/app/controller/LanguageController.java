package com.dvd.rental.app.controller;

import java.net.URI;
import java.util.List;
import jakarta.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.dvd.rental.app.entities.Language;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.LanguageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("/languages")
@Tag(name = "Catalog", description = "Language Catalog CRUD endpoints")
public class LanguageController {
    
    private final LanguageService languageService;
    
    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }
    
    @GetMapping
    @Operation(summary = "Get a list of all languages")
    @ApiResponse(responseCode = "200", description = "Successfully retrieved the list of languages")
    public ResponseEntity<List<Language>> getAllLanguages() {
        List<Language> languages = languageService.getAllLanguages();
        return ResponseEntity.ok(languages);
    }
    
    @GetMapping("/{id}")
    @Operation(summary = "Get a language by ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successfully retrieved the language"),
        @ApiResponse(responseCode = "404", description = "Language not found")
    })
    public ResponseEntity<Language> getLanguageById(@PathVariable @Parameter(description = "Language ID") Integer id) throws DataNotFoundException {
        
        Language language = languageService.getLanguageById(id);
        if (language != null) {
            return new ResponseEntity<>(language, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
    }
    
    @PostMapping
    @Operation(summary = "Create a new language")
    @ApiResponse(responseCode = "201", description = "Language created successfully")
    public ResponseEntity<Language> createLanguage(@Valid @RequestBody Language language) {
        Language createdLanguage = languageService.createLanguage(language);
        return new ResponseEntity<>(createdLanguage, HttpStatus.CREATED);
    }
    
    @PutMapping("/{id}")
    @Operation(summary = "Update a language by ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Language updated successfully"),
        @ApiResponse(responseCode = "404", description = "Language not found")
    })
    public ResponseEntity<Language> updateLanguage(@PathVariable @Parameter(description = "Language ID") Integer id, 
            @Valid @RequestBody Language languageDetails) {
        Language updatedLanguage = languageService.updateLanguage(id, languageDetails);
        if (updatedLanguage != null) {
            return ResponseEntity.ok(updatedLanguage);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a language by ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "204", description = "Language deleted successfully"),
        @ApiResponse(responseCode = "404", description = "Language not found")
    })
    public ResponseEntity<Void> deleteLanguage(@PathVariable @Parameter(description = "Language ID") Integer id) {
        try {
            languageService.deleteLanguage(id);
           
        } catch (DataNotFoundException e) {
            ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    
    }
}
