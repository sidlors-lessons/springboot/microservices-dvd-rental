package com.dvd.rental.app.entities;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class FilmActorPK implements Serializable{

    public FilmActorPK() {
       
    }


    public FilmActorPK(int actorId, int filmId) {
		this.actorId=actorId;
        this.filmId=filmId;
	}

	@Column(name = "actor_id")
    private Integer actorId;

    @Column(name = "film_id")
    private Integer filmId;

    // getters and setters
}

