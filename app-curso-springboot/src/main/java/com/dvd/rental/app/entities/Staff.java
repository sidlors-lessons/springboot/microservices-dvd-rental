package com.dvd.rental.app.entities;

import java.time.LocalDateTime;
import jakarta.persistence.*;
import jakarta.validation.constraints.PositiveOrZero;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "staff")
@Getter
@Setter
@NoArgsConstructor
public class Staff {
    
    public Staff(int staffId, String firstName, String lastName) {
        this.staffId = staffId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "staff_id")
    @PositiveOrZero
    private Integer staffId;
    
    @Column(name = "first_name", nullable = false)
    private String firstName;
    
    @Column(name = "last_name", nullable = false)
    private String lastName;
    
    @Column(name = "address_id", nullable = false)
    private Integer addressId;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "store_id", nullable = false)
    private Integer storeId;
    
    @Column(name = "active", nullable = false)
    private boolean active;
    
    @Column(name = "username", nullable = false)
    private String username;
    
    @Column(name = "password")
    private String password;
    
    @Column(name = "last_update", nullable = false)
    private LocalDateTime lastUpdate;
    
    @Column(name = "picture")
    private byte[] picture;

    // Getters and Setters
    
    // Constructors

    @PrePersist
    private void setCreationTimestamp() {
        lastUpdate = LocalDateTime.now();
    }
}
