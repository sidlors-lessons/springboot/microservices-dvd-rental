package com.dvd.rental.app.repositories;

import com.dvd.rental.app.entities.Rental;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentalRepository extends JpaRepository<Rental, Integer> {
    
    // Optionally, add custom query methods if needed
    
}