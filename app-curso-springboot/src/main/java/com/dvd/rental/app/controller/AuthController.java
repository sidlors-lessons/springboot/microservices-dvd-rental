package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.LdapUser;
import com.dvd.rental.app.service.LdapUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dvd.rental.app.entities.Usuario;
import com.dvd.rental.app.security.JwtTokenProvider;
import com.dvd.rental.app.vo.RequestUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/auth")
@Tag(name = "Auth", description = "Authentication service for dvd rental")

public class AuthController {

    private LdapUserServiceImpl usuarioDetailsService;

    private final JwtTokenProvider jwtTokenProvider;

	

    @Autowired
    public AuthController(LdapUserServiceImpl usuarioDetailsService
    , JwtTokenProvider jwtTokenProvider) {
        this.usuarioDetailsService = usuarioDetailsService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/users/")
    public void saveUsuario(@RequestBody LdapUser user) {
        usuarioDetailsService.createUser(user);
    }

    @GetMapping("/users/")
    public List<LdapUser> getAllUsuarios() {
        return usuarioDetailsService.getAllUsers();
    }

    @GetMapping("/users/{username}")
    public LdapUser getUsuario(@PathVariable String username) throws UsernameNotFoundException {
        LdapUser detail=usuarioDetailsService.getUserByUid(username);
        return new LdapUser(detail.getLastName(),detail.getUserPassword());
    }


    @PostMapping("/token/")
    @Operation(summary = "Autenticar usuario")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Autenticación exitosa"),
			@ApiResponse(responseCode = "401", description = "Credenciales inválidas") })
	public ResponseEntity<Map<String, String>> authenticate(@RequestBody RequestUser request) {


	    if (usuarioDetailsService.validateUserAndPass(request.getUsername(),request.getPassword())) {
	        // Autenticación exitosa. Generar y devolver el token JWT.
	        String token = jwtTokenProvider.generateToken(request.getUsername());
	        Map<String, String> response = new HashMap<>();
	        response.put("token", token);
	        // Crear un objeto Authentication y configurarlo en el contexto de seguridad.
	        Authentication authentication = new UsernamePasswordAuthenticationToken(request.getUsername(), null, new ArrayList<>());
            SecurityContextHolder.getContext().setAuthentication(authentication);
	        return ResponseEntity.ok(response);
	    } else {
	        // Credenciales inválidas.
	        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
	    }
	}

    // Endpoint para eliminar un usuario por su ID
     // Endpoint para eliminar un usuario por su username
     @DeleteMapping("/{username}")
     @Operation(summary = "Eliminar un usuario por su username", security = @SecurityRequirement(name = "bearerAuth"))
     @ApiResponse(responseCode = "200", description = "Usuario eliminado exitosamente")
     @ApiResponse(responseCode = "404", description = "Usuario no encontrado")
     public ResponseEntity<String> eliminarUsuarioPorUsername(@PathVariable String username) {
         // Llamamos al servicio para eliminar el usuario por su username
         boolean usuarioEliminado = usuarioDetailsService.deleteUser(username);
         if (usuarioEliminado) {
             return ResponseEntity.ok("Usuario eliminado exitosamente.");
         } else {
             return ResponseEntity.notFound().build();
         }
     }
}