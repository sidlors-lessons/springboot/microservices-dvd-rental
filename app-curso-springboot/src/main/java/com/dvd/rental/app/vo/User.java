package com.dvd.rental.app.vo;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User {

    @NotNull
    @Pattern(regexp=".+@.+\\.[a-z]+")
    private String email;
    private int edad;
    private String genero;

}
