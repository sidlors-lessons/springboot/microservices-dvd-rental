package com.dvd.rental.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.dvd.rental.app.entities.Category;
import com.dvd.rental.app.service.CategoryService;
import com.dvd.rental.app.vo.CategoryVO;
import java.util.List;
import java.util.Optional;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;


@RestController
@RequestMapping("/categories")
@Tag(name = "Catalog", description = "Catalogs CRUD endpoints")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    @Operation(summary = "Get a list of all categories")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found list of categories",
            content = { @Content(mediaType = "application/json")}),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied", 
            content = @Content), 
        @ApiResponse(responseCode = "404", description = "Categories not found", 
            content = @Content) })
    public ResponseEntity<List<CategoryVO>> getAllCategories() {
        List<CategoryVO> categories = categoryService.getAllCategories();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/{categoryId}")
    @Operation(summary = "Get a category by ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the category",
            content = { @Content(mediaType = "application/json")}),
        @ApiResponse(responseCode = "404", description = "Category not found",
            content = @Content) })
    public ResponseEntity<Category> getCategoryById(
            @Parameter(description = "ID of the category to be obtained") @PathVariable Integer categoryId) {
        Optional<Category> category = categoryService.getCategoryById(categoryId);
        if (category.isPresent()) {
            return new ResponseEntity<>(category.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    @Operation(summary = "Create a new category")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "Category created",
            content = { @Content(mediaType = "application/json")}) })
    public ResponseEntity<Category> createCategory(
            @Parameter(description = "Category object to be created") @RequestBody Category category) {
        Category savedCategory = categoryService.saveCategory(category);
        return new ResponseEntity<>(savedCategory, HttpStatus.CREATED);
    }

    @PutMapping("/{categoryId}")
    @Operation(summary = "Update a category by ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Category updated",
            content = { @Content(mediaType = "application/json")}),
        @ApiResponse(responseCode = "404", description = "Category not found",
            content = @Content) })
    public ResponseEntity<Category> updateCategory(
            @Parameter(description = "ID of the category to be updated") @PathVariable Integer categoryId,
            @Parameter(description = "Updated category object") @RequestBody Category category) {
        Optional<Category> currentCategory = categoryService.getCategoryById(categoryId);
        if (currentCategory.isPresent()) {
            category.setCategoryId(categoryId);
            Category updatedCategory = categoryService.saveCategory(category);
            return new ResponseEntity<>(updatedCategory, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}