package com.dvd.rental.app.service;

import com.dvd.rental.app.entities.Address;
import com.dvd.rental.app.repositories.AddressRepository;

import java.util.List;


import com.dvd.rental.app.service.AddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class AddressService {

    private final AddressRepository addressRepository;

    @Autowired
    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<Address> getAllAddresses() {
        return addressRepository.findAll();
    }

    public Address getAddressById(Integer addressId) {
        Optional<Address> optionalAddress = addressRepository.findById(addressId);
        return optionalAddress.orElse(null);
    }
    public Address createAddress(Address address) {
        return addressRepository.save(address);
    }

    public Address updateAddress(Integer addressId, Address addressDetails) {
        Address existingAddress = getAddressById(addressId);
        if (existingAddress != null) {
            existingAddress.setAddress(addressDetails.getAddress());
            existingAddress.setAddress2(addressDetails.getAddress2());
            existingAddress.setDistrict(addressDetails.getDistrict());
            existingAddress.setCityId(addressDetails.getCityId());
            existingAddress.setPostalCode(addressDetails.getPostalCode());
            existingAddress.setPhone(addressDetails.getPhone());
            return addressRepository.save(existingAddress);
        }
        return null;
    }

    public boolean deleteAddress(Integer addressId) {
        Address existingAddress = getAddressById(addressId);
        if (existingAddress != null) {
            addressRepository.delete(existingAddress);
            return true;
        }
        return false;
    }
}
