package com.dvd.rental.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.dvd.rental.app.entities.Actor;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.ActorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;



@RestController
@RequestMapping("/actors")
@Tag(name = "Catalog", description = "Catalogs CRUD endpoints")
public class ActorController {

    private final ActorService actorService;

    @Autowired
    public ActorController(ActorService actorService) {
        this.actorService = actorService;
    }

    @GetMapping
    @Operation(summary = "Get a list of all actors")
    @ApiResponse(responseCode = "200", description = "Found list of actors",
            content = { @Content(mediaType = "application/json") })
    public ResponseEntity<List<Actor>> getAllActors() {
        List<Actor> actors = actorService.findAll();
        return new ResponseEntity<>(actors, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get an actor by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the actor",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "Actor not found",
                    content = @Content)
    })
    public ResponseEntity<Actor> getActorById(@PathVariable @Parameter(description = "Actor ID") Integer id) throws DataNotFoundException {
        Actor actor = actorService.findById(id);
        if (actor != null) {
            return new ResponseEntity<>(actor, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new actor")
    @ApiResponse(responseCode = "201", description = "Actor created",
            content = { @Content(mediaType = "application/json") })
    public ResponseEntity<Actor> createActor(@RequestBody @Parameter(description = "Actor object") Actor actor) {
        Actor createdActor = actorService.save(actor);
        return new ResponseEntity<>(createdActor, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete an actor by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Actor deleted"),
            @ApiResponse(responseCode = "404", description = "Actor not found",
                    content = @Content)
    })
    public ResponseEntity<Void> deleteActorById(@PathVariable @Parameter(description = "Actor ID") Integer id) {
        
        try {
            actorService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
       
    }
}
