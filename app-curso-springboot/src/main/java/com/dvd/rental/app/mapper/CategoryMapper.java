package com.dvd.rental.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.dvd.rental.app.entities.Category;
import com.dvd.rental.app.vo.CategoryVO;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    CategoryMapper INSTANCIA= Mappers.getMapper(CategoryMapper.class);

    CategoryVO toVO(Category   entity);
    Category toEntity(CategoryVO vo);
    
}
