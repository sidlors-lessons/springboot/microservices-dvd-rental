package com.dvd.rental.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dvd.rental.app.entities.Language;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Integer> {
}
