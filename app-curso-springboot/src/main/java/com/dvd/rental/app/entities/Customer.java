package com.dvd.rental.app.entities;

import java.sql.Timestamp;
import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "customer")
@Getter
@Setter
public class Customer {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Integer id;
    
    @Column(name = "store_id", nullable = false)
    private Short storeId;
    
    @Column(name = "first_name", nullable = false)
    private String firstName;
    
    @Column(name = "last_name", nullable = false)
    private String lastName;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "address_id", nullable = false)
    private Short addressId;
    
    @Column(name = "activebool", nullable = false)
    private boolean activebool;
    
    @Column(name = "create_date", nullable = false)
    private Date createDate;
    
    @Column(name = "last_update", nullable = false)
    private Timestamp lastUpdate;
    
    @Column(name = "active")
    private Integer active;
    
    // constructor, getters and setters
}