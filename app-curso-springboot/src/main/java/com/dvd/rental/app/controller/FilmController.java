package com.dvd.rental.app.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.dvd.rental.app.entities.Film;
import com.dvd.rental.app.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/films")
@Tag(name = "Catalog", description = "Catalogs CRUD endpoints")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @GetMapping
    @Operation(summary = "Get a list of all films")
    public ResponseEntity<List<Film>> getAllFilms() {
        List<Film> films = filmService.getAllFilms();
        return new ResponseEntity<>(films, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a film by ID")
    public ResponseEntity<Film> getFilmById(@PathVariable @io.swagger.v3.oas.annotations.Parameter(description = "Film ID") Integer id) {
        Optional<Film> optionalFilm = filmService.getFilmById(id);
        if (optionalFilm.isPresent()) {
            return new ResponseEntity<>(optionalFilm.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    @Operation(summary = "Create a new film")
    public ResponseEntity<Film> createFilm(@RequestBody Film film) {
        Film createdFilm = filmService.createFilm(film);
        return ResponseEntity.created(URI.create("/films/" + createdFilm.getFilmId())).body(createdFilm);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a film by ID")
    public ResponseEntity<Void> deleteFilm(@PathVariable @io.swagger.v3.oas.annotations.Parameter(description = "Film ID") Integer id) {
        filmService.deleteFilm(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
