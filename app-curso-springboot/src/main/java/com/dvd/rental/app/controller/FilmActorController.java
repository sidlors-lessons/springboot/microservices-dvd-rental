package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.FilmActor;
import com.dvd.rental.app.entities.FilmActorPK;
import com.dvd.rental.app.service.FilmActorService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/film-actors")
@Tag(name = "Catalog", description = "Relational Catalogs")
public class FilmActorController {
    @Autowired
    private FilmActorService filmActorService;

    @GetMapping
    @Operation(summary = "Get a list of all film actors")
    public ResponseEntity<List<FilmActor>> getAllFilmActors() {
        List<FilmActor> filmActors = filmActorService.getAllFilmActors();
        return new ResponseEntity<>(filmActors, HttpStatus.OK);
    }

    @GetMapping("/{actorId}/{filmId}")
    @Operation(summary = "Get a film actor by actor ID and film ID")
    public ResponseEntity<FilmActor> getFilmActorById(
            @PathVariable("actorId") @Parameter(description = "Actor ID") Short actorId,
            @PathVariable("filmId") @Parameter(description = "Film ID") Short filmId
    ) {
        Optional<FilmActor> filmActor = filmActorService.getFilmActorById(new FilmActorPK(actorId, filmId));
        return filmActor.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @Operation(summary = "Create a new film actor")
    public ResponseEntity<FilmActor> saveFilmActor(@RequestBody FilmActor filmActor) {
        FilmActor savedFilmActor = filmActorService.saveFilmActor(filmActor);
        return new ResponseEntity<>(savedFilmActor, HttpStatus.CREATED);
    }

    @DeleteMapping("/{actorId}/{filmId}")
    @Operation(summary = "Delete a film actor by actor ID and film ID")
    public ResponseEntity<Void> deleteFilmActor(
            @PathVariable("actorId") @Parameter(description = "Actor ID") Short actorId,
            @PathVariable("filmId") @Parameter(description = "Film ID") Short filmId
    ) {
        filmActorService.deleteFilmActor(new FilmActorPK(actorId, filmId));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
