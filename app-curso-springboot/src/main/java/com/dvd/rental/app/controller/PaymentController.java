package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Payment;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.PaymentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.media.Content;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;

@RestController
@RequestMapping("/payments")
@Tag(name = "Operational", description = "Payment API endpoints")
public class PaymentController {

    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping
    @Operation(summary = "Get a paginated list of payments")
    @ApiResponse(responseCode = "200", description = "Paginated list of payments", content = {
            @Content(mediaType = "application/json")
    })
    public ResponseEntity<Page<Payment>> getPaginatedPayments(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size
    ) {
        Page<Payment> payments = paymentService.getPaginatedPayments(page, size);
        return ResponseEntity.ok(payments);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a payment by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the payment", content = {
                    @Content(mediaType = "application/json")
            }),
            @ApiResponse(responseCode = "404", description = "Payment not found", content = @Content)
    })
    public ResponseEntity<Payment> getPaymentById(
            @PathVariable @Parameter(description = "Payment ID") Integer id
    ) {
        try {
            Payment payment = paymentService.getPaymentById(id);
            return ResponseEntity.ok(payment);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        } 
    }

    @PostMapping
    @Operation(summary = "Create a new payment")
    @ApiResponse(responseCode = "201", description = "Payment created", content = {
            @Content(mediaType = "application/json")
    })
    public ResponseEntity<Payment> createPayment(@RequestBody Payment payment) {
        Payment createdPayment = paymentService.savePayment(payment);
        URI location = URI.create("/payments/" + createdPayment.getPaymentId());
        return ResponseEntity.created(location).body(createdPayment);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a payment by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Payment deleted"),
            @ApiResponse(responseCode = "404", description = "Payment not found", content = @Content)
    })
    public ResponseEntity<Void> deletePayment(
            @PathVariable @Parameter(description = "Payment ID") Integer id
    ) {
        try {
            paymentService.deletePayment(id);
           
        } catch (DataNotFoundException e) {
            ResponseEntity.notFound().build();
            
        }
        return ResponseEntity.noContent().build();
    }
}