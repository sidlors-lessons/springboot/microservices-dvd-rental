package com.dvd.rental.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dvd.rental.app.entities.Country;
import com.dvd.rental.app.repositories.CountryRepository;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    @Override
    public Country getCountryById(Integer id) {
        return countryRepository.findById(id).orElse(null);
    }

    @Override
    public Country addCountry(Country country) {
        return countryRepository.save(country);
    }

    @Override
    public Country updateCountry(Integer id, Country country) {
        Country countryToUpdate = getCountryById(id);
        if (countryToUpdate == null) {
            return null;
        }
        countryToUpdate.setCountry(country.getCountry());
        countryToUpdate.setLastUpdate(country.getLastUpdate());
        return countryRepository.save(countryToUpdate);
    }

    @Override
    public void deleteCountry(Integer id) {
        countryRepository.deleteById(id);
    }
}
