package com.dvd.rental.app.repositories;

import com.dvd.rental.app.entities.FilmCategory;
import com.dvd.rental.app.entities.FilmCategoryPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmCategoryRepository extends JpaRepository<FilmCategory, FilmCategoryPK> {
}
