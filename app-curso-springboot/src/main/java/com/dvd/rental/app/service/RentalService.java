package com.dvd.rental.app.service;

import com.dvd.rental.app.entities.Rental;
import com.dvd.rental.app.repositories.RentalRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RentalService {
    
    private final RentalRepository rentalRepository;
    
    public RentalService(RentalRepository rentalRepository) {
        this.rentalRepository = rentalRepository;
    }
    
    public List<Rental> getAllRentals() {
        return rentalRepository.findAll();
    }
    
    public Optional<Rental> getRentalById(Integer rentalId) {
        return rentalRepository.findById(rentalId);
    }
    
    public Rental createRental(Rental rental) {
        return rentalRepository.save(rental);
    }
    
    public Rental updateRental(Integer rentalId, Rental rentalDetails) {
        Optional<Rental> optionalRental = rentalRepository.findById(rentalId);
        if (optionalRental.isPresent()) {
            Rental rental = optionalRental.get();
            rental.setRentalDate(rentalDetails.getRentalDate());
            rental.setInventoryId(rentalDetails.getInventoryId());
            rental.setCustomerId(rentalDetails.getCustomerId());
            rental.setReturnDate(rentalDetails.getReturnDate());
            rental.setStaffId(rentalDetails.getStaffId());
            rental.setLastUpdate(rentalDetails.getLastUpdate());
            return rentalRepository.save(rental);
        } else {
            throw new IllegalArgumentException("Rental not found with ID: " + rentalId);
        }
    }
    
    public void deleteRental(Integer rentalId) {
        rentalRepository.deleteById(rentalId);
    }

    public Page<Rental> getPaginatedPayments(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return rentalRepository.findAll(pageable);
    }
}
