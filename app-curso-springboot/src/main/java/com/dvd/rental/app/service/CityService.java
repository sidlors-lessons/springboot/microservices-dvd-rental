package com.dvd.rental.app.service;


import com.dvd.rental.app.entities.City;
import com.dvd.rental.app.repositories.CityRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityService {

    private final CityRepository cityRepository;

    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public List<City> getAllCities() {
        return cityRepository.findAll();
    }

    public Optional<City> getCityById(Integer cityId) {
        return cityRepository.findById(cityId);
    }

    public City createCity(City city) {
        return cityRepository.save(city);
    }

    public City updateCity(Integer cityId, City cityDetails) {
        Optional<City> optionalCity = cityRepository.findById(cityId);
        if (optionalCity.isPresent()) {
            City city = optionalCity.get();
            // Update city details
            city.setCity(cityDetails.getCity());
            city.setCountryId(cityDetails.getCountryId());
            city.setLastUpdate(cityDetails.getLastUpdate());
            return cityRepository.save(city);
        }
        return null;
    }

    public boolean deleteCity(Integer cityId) {
        Optional<City> optionalCity = cityRepository.findById(cityId);
        if (optionalCity.isPresent()) {
            cityRepository.delete(optionalCity.get());
            return true;
        }
        return false;
    }
}
