package com.dvd.rental.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dvd.rental.app.entities.Actor;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.repositories.ActorRepository;

@Service
public class ActorService {

    private final ActorRepository actorRepository;

    @Autowired
    public ActorService(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }

    public List<Actor> findAll() {
        return actorRepository.findAll();
    }

    public Actor findById(Integer actorId) throws DataNotFoundException{
        return actorRepository.findById(actorId).orElseThrow(() -> new DataNotFoundException( "actorId"));
    }

    public Actor save(Actor actor) {
        return actorRepository.save(actor);
    }

    public void deleteById(Integer actorId) throws DataNotFoundException {
        if (!actorRepository.existsById(actorId)) {
            throw new DataNotFoundException("id");
        }
        actorRepository.deleteById(actorId);
    }
}