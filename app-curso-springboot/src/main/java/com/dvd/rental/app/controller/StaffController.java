package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Staff;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.StaffService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/staff")
@Tag(name = "Catalog", description = "Staff Operations")
public class StaffController {

    private final StaffService staffService;

    public StaffController(StaffService staffService) {
        this.staffService = staffService;
    }

    @GetMapping
    @Operation(summary = "Get all staff members dvd rental app")
    @ApiResponse(responseCode = "200", description = "Found list of staff members", content = {
            @Content(mediaType = "application/json") })
    public ResponseEntity<List<Staff>> getAllStaff() {
        List<Staff> staffList = staffService.getAllStaff();
        return ResponseEntity.ok(staffList);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a staff member by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the staff member", content = {
                    @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "Staff member not found", content = @Content)
    })
    public ResponseEntity<Staff> getStaffById(
            @PathVariable @Parameter(description = "Staff ID Non-negative integer parameter", schema = @Schema(type = "integer", minimum = "0")) Integer id) {
        Staff staff;
        try {
                staff = staffService.getStaffById(id);
                return ResponseEntity.ok(staff);
        } catch (DataNotFoundException e) {
                return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create a new staff member")
    @ApiResponse(responseCode = "200", description = "Staff member created", content = {
            @Content(mediaType = "application/json") })
    public ResponseEntity<Staff> createStaff(@Valid @RequestBody Staff staff) {
        Staff createdStaff = staffService.createStaff(staff);
        return ResponseEntity.ok(createdStaff);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a staff member by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Staff member updated", content = {
                    @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "Staff member not found", content = @Content)
    })
    public ResponseEntity<Staff> updateStaff(
            @PathVariable @Parameter(description = "Staff ID") Integer id,
            @Valid @RequestBody Staff staff) throws DataNotFoundException {
        Staff updatedStaff = staffService.updateStaff(id, staff);
        return ResponseEntity.ok(updatedStaff);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a staff member by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Staff member deleted"),
            @ApiResponse(responseCode = "404", description = "Staff member not found", content = @Content)
    })
    public ResponseEntity<Void> deleteStaff(
            @PathVariable @Parameter(description = "Staff ID") Integer id) throws DataNotFoundException {
        staffService.deleteStaff(id);
        return ResponseEntity.noContent().build();
    }
}
