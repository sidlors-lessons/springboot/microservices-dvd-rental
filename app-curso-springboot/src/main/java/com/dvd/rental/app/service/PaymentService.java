package com.dvd.rental.app.service;

import com.dvd.rental.app.entities.Payment;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.repositories.PaymentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {

    private final PaymentRepository paymentRepository;

    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }


    public Page<Payment> getPaginatedPayments(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return paymentRepository.findAll(pageable);
    }

    public Payment getPaymentById(Integer id) throws DataNotFoundException {
        return paymentRepository.findById(id).orElseThrow(() -> new DataNotFoundException( "payament id not found"));
    }

    public Payment savePayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    public void deletePayment(Integer id) throws DataNotFoundException {

        if (!paymentRepository.existsById(id)) {
            throw new DataNotFoundException(" payment Id");
        }
        paymentRepository.deleteById(id);
    }
}
