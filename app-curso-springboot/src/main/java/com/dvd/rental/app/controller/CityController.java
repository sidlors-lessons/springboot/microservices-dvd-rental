package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.City;
import com.dvd.rental.app.service.CityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cities")
@Tag(name = "Catalog", description = "City CRUD Endpoints")
public class CityController {

    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    @Operation(summary = "Get all cities")
    @ApiResponse(responseCode = "200", description = "Success")
    public ResponseEntity<List<City>> getAllCities() {
        List<City> cities = cityService.getAllCities();
        return ResponseEntity.ok(cities);
    }

    @GetMapping("/{cityId}")
    @Operation(summary = "Get city by ID")
    @ApiResponse(responseCode = "200", description = "Success")
    @ApiResponse(responseCode = "404", description = "City not found")
    public ResponseEntity<City> getCityById(
            @PathVariable
            @Parameter(description = "City ID", example = "1")
                    Integer cityId) {
        Optional<City> optionalCity = cityService.getCityById(cityId);
        if (optionalCity.isPresent()) {
            return ResponseEntity.ok(optionalCity.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create a new city")
    @ApiResponse(responseCode = "201", description = "City created")
    public ResponseEntity<City> createCity(
            @Valid
            @RequestBody
            @Parameter(description = "City object", required = true, schema = @Schema(implementation = City.class))
                    City city) {
        City createdCity = cityService.createCity(city);
        URI location = URI.create("/cities/" + createdCity.getCityId());
        return ResponseEntity.created(location).body(createdCity);
    }

    @PutMapping("/{cityId}")
    @Operation(summary = "Update city by ID")
    @ApiResponse(responseCode = "200", description = "City updated")
    @ApiResponse(responseCode = "404", description = "City not found")
    public ResponseEntity<City> updateCity(
            @PathVariable
            @Parameter(description = "City ID", example = "1")
                    Integer cityId,
            @Valid
            @RequestBody
            @Parameter(description = "City object", required = true, schema = @Schema(implementation = City.class))
                    City cityDetails) {
        City updatedCity = cityService.updateCity(cityId, cityDetails);
        if (updatedCity != null) {
            return ResponseEntity.ok(updatedCity);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{cityId}")
    @Operation(summary = "Delete city by ID")
    @ApiResponse(responseCode = "204", description = "City deleted")
    @ApiResponse(responseCode = "404", description = "City not found")
    public ResponseEntity<Void> deleteCity(
            @PathVariable
            @Parameter(description = "City ID", example = "1")
                    Integer cityId) {
        boolean deleted = cityService.deleteCity(cityId);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
                    
}