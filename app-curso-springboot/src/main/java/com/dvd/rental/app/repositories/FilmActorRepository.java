package com.dvd.rental.app.repositories;

import com.dvd.rental.app.entities.FilmActor;
import com.dvd.rental.app.entities.FilmActorPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmActorRepository extends JpaRepository<FilmActor, FilmActorPK> {
}