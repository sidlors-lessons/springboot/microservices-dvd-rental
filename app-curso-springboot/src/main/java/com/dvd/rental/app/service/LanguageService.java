package com.dvd.rental.app.service;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.stereotype.Service;
import com.dvd.rental.app.entities.Language;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.repositories.LanguageRepository;

@Service
public class LanguageService {
    
    private final LanguageRepository languageRepository;
    
    public LanguageService(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }
    
    public List<Language> getAllLanguages() {
        return languageRepository.findAll();
    }
    
    public Language getLanguageById(Integer id) throws DataNotFoundException {

        if (!languageRepository.existsById(id)) {
            throw new DataNotFoundException("id");
        }
        return languageRepository.getReferenceById(id);
    }
    
    public Language createLanguage(Language language) {
        return languageRepository.save(language);
    }
    
    public Language updateLanguage(Integer id, Language languageDetails) {
        Language language = languageRepository.findById(id).get();
                //.orElseThrow(() -> new ResourceNotFoundException("Language", "id", id));
        
        language.setName(languageDetails.getName());
        language.setLastUpdate(LocalDateTime.now());
        
        return languageRepository.save(language);
    }
    
    public void deleteLanguage(Integer id) throws DataNotFoundException {

        if (languageRepository.existsById(id)) {
            throw new DataNotFoundException("id");
        }
        languageRepository.deleteById(id);
    }
}
