package com.dvd.rental.app.service;

import com.dvd.rental.app.entities.FilmActor;
import com.dvd.rental.app.entities.FilmActorPK;
import com.dvd.rental.app.repositories.FilmActorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class FilmActorService {
	@Autowired
	private FilmActorRepository filmActorRepository;

	public List<FilmActor> getAllFilmActors() {
		return filmActorRepository.findAll();
	}

	public Optional<FilmActor> getFilmActorById(FilmActorPK id) {
		return filmActorRepository.findById(id);
	}

	public FilmActor saveFilmActor(FilmActor filmActor) {
		return filmActorRepository.save(filmActor);
	}

	public void deleteFilmActor(FilmActorPK id) {
		filmActorRepository.deleteById(id);
	}
}