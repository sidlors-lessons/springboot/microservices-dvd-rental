package com.dvd.rental.app.controller;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.dvd.rental.app.entities.Language;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.LanguageService;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class LanguageControllerTest {

    private MockMvc mockMvc;

    @Mock
    private LanguageService languageService;

    @InjectMocks
    private LanguageController languageController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(languageController).build();
    }

    @Test
    void getAllLanguages_ShouldReturnListOfLanguages() {
        // Arrange
        List<Language> languages = Arrays.asList(
                new Language(1, "English"),
                new Language(2, "Spanish")
        );

        when(languageService.getAllLanguages()).thenReturn(languages);

        // Act
        ResponseEntity<List<Language>> response = languageController.getAllLanguages();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(languages, response.getBody());
        verify(languageService, times(1)).getAllLanguages();
    }

    @Test
    void getLanguageById_WhenLanguageExists_ShouldReturnLanguage() throws DataNotFoundException {
        // Arrange
        int languageId = 1;
        Language language = new Language(languageId, "English");

        when(languageService.getLanguageById(languageId)).thenReturn(language);

        // Act
        ResponseEntity<Language> response = languageController.getLanguageById(languageId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(language, response.getBody());
        verify(languageService, times(1)).getLanguageById(languageId);
    }

    @Test
    void getLanguageById_WhenLanguageDoesNotExist_ShouldReturnNotFound() throws DataNotFoundException {
        // Arrange
        int languageId = 1;

        when(languageService.getLanguageById(languageId)).thenReturn(null);

        // Act
        ResponseEntity<Language> response = languageController.getLanguageById(languageId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
        verify(languageService, times(1)).getLanguageById(languageId);
    }

    @Test
    void createLanguage_ShouldReturnCreatedLanguage() {
        // Mock data
        Language language = new Language(1, "English");

        // Mock service method
        when(languageService.createLanguage(language)).thenReturn(language);

        // Call the controller method
        ResponseEntity<Language> response = languageController.createLanguage(language);

        // Verify the response
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(language, response.getBody());

        // Verify that the service method was called
        verify(languageService, times(1)).createLanguage(language);
    }



     // Prueba unitaria para updateLanguage

     @Test
     void updateLanguage_WhenLanguageExists_ShouldReturnUpdatedLanguage() {
         // Arrange
         int languageId = 1;
         Language languageDetails = new Language(languageId, "English");
 
         Language updatedLanguage = new Language(languageId, "Updated English");
 
         when(languageService.updateLanguage(languageId, languageDetails)).thenReturn(updatedLanguage);
 
         // Act
         ResponseEntity<Language> response = languageController.updateLanguage(languageId, languageDetails);
 
         // Assert
         assertEquals(HttpStatus.OK, response.getStatusCode());
         assertEquals(updatedLanguage, response.getBody());
         verify(languageService, times(1)).updateLanguage(languageId, languageDetails);
     }
 
     @Test
     void updateLanguage_WhenLanguageDoesNotExist_ShouldReturnNotFound() {
         // Arrange
         int languageId = 1;
         Language languageDetails = new Language(languageId, "English");
 
         when(languageService.updateLanguage(languageId, languageDetails)).thenReturn(null);
 
         // Act
         ResponseEntity<Language> response = languageController.updateLanguage(languageId, languageDetails);
 
         // Assert
         assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
         assertNull(response.getBody());
         verify(languageService, times(1)).updateLanguage(languageId, languageDetails);
     }
 
     // Prueba unitaria para deleteLanguage
 
     @Test
     void deleteLanguage_WhenLanguageExists_ShouldReturnNoContent() throws DataNotFoundException {
         // Arrange
         int languageId = 1;
 
         // Act
         ResponseEntity<Void> response = languageController.deleteLanguage(languageId);
 
         // Assert
         assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
         verify(languageService, times(1)).deleteLanguage(languageId);
     }
 
     @Test
     void deleteLanguage_WhenLanguageDoesNotExist_ShouldReturnNotFound() throws DataNotFoundException {
         // Arrange
         int languageId = 0;
 
         // Mock service method to throw DataNotFoundException
         doThrow(DataNotFoundException.class).when(languageService).deleteLanguage(languageId);
 
         // Act
         ResponseEntity<Void> response = languageController.deleteLanguage(languageId);
 
         // Assert
         assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
         verify(languageService, times(1)).deleteLanguage(languageId);
     }

}