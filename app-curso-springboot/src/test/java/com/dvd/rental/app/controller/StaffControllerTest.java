package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Staff;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.StaffService;

import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.*;

@Slf4j
class StaffControllerTest {

    
    private MockMvc mockMvc;

    @Mock
    private StaffService staffService;

    @InjectMocks
    private StaffController staffController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(staffController).build();
    }

    @Test
    void getAllStaff_ShouldReturnListOfStaff() throws Exception {
        // Arrange
        List<Staff> staffList = Arrays.asList(
                new Staff(1, "John", "Dow"),
                new Staff(2, "Jane", "Smith"));
        when(staffService.getAllStaff()).thenReturn(staffList);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/staff"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(staffList.size()));
    }

    @Test
    void getStaffById_WhenStaffExists_ShouldReturnStaff() throws Exception {
        // Arrange
        int staffId = 1;
        Staff staff = new Staff(staffId, "John", "Doe");
        when(staffService.getStaffById(staffId)).thenReturn(staff);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/staff/{id}", staffId))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.staffId").value(staffId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(staff.getLastName()));
    }

    @Test
    void getStaffById_WhenStaffMemberDoesNotExist_ShouldReturnNotFound() throws DataNotFoundException {
        // Arrange
        int staffId = 1;

        when(staffService.getStaffById(staffId)).thenThrow(new DataNotFoundException("Staff member not found"));

        // Act
        ResponseEntity<Staff> response = staffController.getStaffById(staffId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(staffService, times(1)).getStaffById(staffId);
    }

    @Test
    void createStaff_ShouldReturnCreatedStaffMember() {
        // Mock data
        Staff staff = new Staff(1, "John", "Doe");

        // Mock service method
        when(staffService.createStaff(staff)).thenReturn(staff);

        // Call the controller method
        ResponseEntity<Staff> response = staffController.createStaff(staff);

        // Verify the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(staff, response.getBody());

        // Verify that the service method was called
        verify(staffService, times(1)).createStaff(staff);
    }


    @Test
    void updateStaff_WhenStaffMemberExists_ShouldReturnUpdatedStaff() throws DataNotFoundException {
        // Arrange
        int staffId = 1;
        Staff existingStaff = new Staff(staffId, "John", "Doe");
        Staff updatedStaff = new Staff(staffId, "Jane", "Smith");
    
        when(staffService.updateStaff(staffId, updatedStaff)).thenReturn(updatedStaff);
    
        // Act
        ResponseEntity<Staff> response = staffController.updateStaff(staffId, updatedStaff);
    
        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedStaff, response.getBody());
        verify(staffService, times(1)).updateStaff(staffId, updatedStaff);
    }

    @Test
    void deleteStaff_ShouldReturnNoContent() throws Exception {
        // Arrange
        int staffId = 1;

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/staff/{id}", staffId))
                .andExpect(status().isNoContent());
    }

}