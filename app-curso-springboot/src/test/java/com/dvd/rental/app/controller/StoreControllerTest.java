package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Store;
import com.dvd.rental.app.service.StoreService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StoreControllerTest {

    private MockMvc mockMvc;

    @Mock
    private StoreService storeService;

    @InjectMocks
    private StoreController storeController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(storeController).build();
    }

    @Test
    void getAllStores_ReturnsListOfStores() throws Exception {
        List<Store> stores = new ArrayList<>();
        stores.add(new Store(1, 1, 1, null));

        when(storeService.getAllStores()).thenReturn(stores);

        mockMvc.perform(MockMvcRequestBuilders.get("/stores"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()").value(stores.size()));
    }

    @Test
    void getStoreById_ValidId_ReturnsStore() throws Exception {
        Store store = new Store(1, 1, 1, null);

        when(storeService.getStoreById(1)).thenReturn(Optional.of(store));

        mockMvc.perform(MockMvcRequestBuilders.get("/stores/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.storeId").value(store.getStoreId()));
    }

    @Test
    void getStoreById_InvalidId_ReturnsNotFound() throws Exception {
        when(storeService.getStoreById(1)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/stores/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void createStore_ValidStore_ReturnsCreated() throws Exception {
        Store store = new Store(1, 1, 1, null);

        when(storeService.createStore(any(Store.class))).thenReturn(store);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/stores")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"managerStaffId\":1,\"addressId\":1}");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.storeId").value(store.getStoreId()));
    }

    @Test
    void updateStore_ValidStore_ReturnsUpdatedStore() throws Exception {
        Store store = new Store(1, 1, 1, null);

        when(storeService.updateStore(eq(1), any(Store.class))).thenReturn(store);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/stores/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"managerStaffId\":1,\"addressId\":1}");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.storeId").value(store.getStoreId()));
    }

    @Test
    void deleteStore_WhenStoreExists_ShouldReturnNoContent() {
        // Arrange
        int storeId = 1;
        Store store = new Store();
        store.setStoreId(storeId);

        when(storeService.getStoreById(storeId)).thenReturn(Optional.of(store));

        // Act
        ResponseEntity<Void> response = storeController.deleteStore(storeId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(storeService, times(1)).deleteStore(storeId);
    }

    @Test
    void deleteStore_WhenStoreDoesNotExist_ShouldReturnNotFound() {
        // Arrange
        int storeId = 1;

        when(storeService.getStoreById(storeId)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<Void> response = storeController.deleteStore(storeId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(storeService, times(1)).deleteStore(storeId);
    }

}