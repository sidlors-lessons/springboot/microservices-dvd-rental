package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Payment;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.net.URI;
import java.util.Collections;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class PaymentControllerTest {

    @Mock
    private PaymentService paymentService;

    @InjectMocks
    private PaymentController paymentController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getPaginatedPayments_ShouldReturnPaginatedPayments() {
        // Arrange
        int page = 0;
        int size = 10;
        Page<Payment> paymentPage = new PageImpl<>(Collections.emptyList());

        when(paymentService.getPaginatedPayments(page, size)).thenReturn(paymentPage);

        // Act
        ResponseEntity<Page<Payment>> response = paymentController.getPaginatedPayments(page, size);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(paymentPage, response.getBody());
        verify(paymentService, times(1)).getPaginatedPayments(page, size);
    }



    @Test
    void createPayment_ShouldCreatePayment() {
        // Arrange
        Payment payment = new Payment();
        payment.setPaymentId(1);

        when(paymentService.savePayment(payment)).thenReturn(payment);

        // Act
        ResponseEntity<Payment> response = paymentController.createPayment(payment);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(payment, response.getBody());
        assertEquals(URI.create("/payments/1"), response.getHeaders().getLocation());
        verify(paymentService, times(1)).savePayment(payment);
    }

    @Test
    void deletePayment_WhenPaymentExists_ShouldReturnNoContent() throws DataNotFoundException {
        // Arrange
        int paymentId = 1;

        // Act
        ResponseEntity<Void> response = paymentController.deletePayment(paymentId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(paymentService, times(1)).deletePayment(paymentId);
    }

    @Test
    void getPaymentById_WhenPaymentExists_ShouldReturnPayment() throws DataNotFoundException {
        // Arrange
        int paymentId = 1;
        Payment payment = new Payment(paymentId);

        when(paymentService.getPaymentById(paymentId)).thenReturn(payment);

        // Act
        ResponseEntity<Payment> response = paymentController.getPaymentById(paymentId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(payment, response.getBody());
        verify(paymentService, times(1)).getPaymentById(paymentId);
    }

    @Test
    void getPaymentById_WhenPaymentDoesNotExist_ShouldReturnNotFound() throws DataNotFoundException {
        // Arrange
        int paymentId = 1;

        when(paymentService.getPaymentById(paymentId)).thenThrow(DataNotFoundException.class);

        // Act
        ResponseEntity<Payment> response = paymentController.getPaymentById(paymentId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(paymentService, times(1)).getPaymentById(paymentId);
    }

    // Add more tests for other controller methods
}
