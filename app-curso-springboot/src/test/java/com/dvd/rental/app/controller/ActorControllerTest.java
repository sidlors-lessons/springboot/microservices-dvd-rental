package com.dvd.rental.app.controller;

import com.dvd.rental.app.entities.Actor;
import com.dvd.rental.app.exception.DataNotFoundException;
import com.dvd.rental.app.service.ActorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ActorControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ActorService actorService;

    @InjectMocks
    private ActorController actorController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(actorController).build();
    }

    @Test
    void getAllActors_ShouldReturnListOfActors() {
        // Arrange
        List<Actor> actors = Arrays.asList(
                new Actor(1, "John", "Doe"),
                new Actor(2, "Jane", "Smith")
        );

        when(actorService.findAll()).thenReturn(actors);

        // Act
        ResponseEntity<List<Actor>> response = actorController.getAllActors();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(actors, response.getBody());
        verify(actorService, times(1)).findAll();
    }

    @Test
    void getActorById_WhenActorExists_ShouldReturnActor() throws DataNotFoundException {
        // Arrange
        int actorId = 1;
        Actor actor = new Actor(actorId, "John", "Doe");

        when(actorService.findById(actorId)).thenReturn(actor);
        

        // Act
        Actor response = actorController.getActorById(actorId).getBody();

        // Assert
        assertEquals(actor, response);
        verify(actorService, times(1)).findById(actorId);
    }

    @Test
    void getActorById_WhenActorDoesNotExist_ShouldReturnNotFound() throws DataNotFoundException {
        // Arrange
        int actorId = 1;

        when(actorService.findById(actorId)).thenReturn(null);

        // Act
        Actor response = actorController.getActorById(actorId).getBody();

        // Assert
        assertEquals(null, response);
        verify(actorService, times(1)).findById(actorId);
    }

    @Test
    void createActor_ShouldReturnCreatedActor() {
        // Mock data
        Actor actor = new Actor(1, "John","Doe");

        // Mock service method
        when(actorService.save(actor)).thenReturn(actor);

        // Call the controller method
        ResponseEntity<Actor> response = actorController.createActor(actor);

        // Verify the response
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(actor, response.getBody());

        // Verify that the service method was called
        verify(actorService, times(1)).save(actor);
    }

    @Test
    void deleteActorById_WhenActorExists_ShouldReturnNoContent() throws DataNotFoundException {
        // Arrange
        int actorId = 1;

        // Act
        ResponseEntity<Void> response = actorController.deleteActorById(actorId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(actorService, times(1)).deleteById(actorId);
    }
}
