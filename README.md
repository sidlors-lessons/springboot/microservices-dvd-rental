## Este proyecto


consta de 2 modules:

 - app-curso-springboot
 - db
 - adminer

para correr, primero hacemos un update la imagen del aplicativo spring

```sh
docker compose build
```

Levantamos acutalizamos el stack

```sh
docker compose up -d
```

finalmente, para consultar correctamente vamos al api seguido de utilizar el enpoint de seguridad 

http://localhost:9988/swagger-ui/index.html


en phpldapadmin

user: cn=admin,dc=sidlors,dc=com
pass: admin


